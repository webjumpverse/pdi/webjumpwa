describe('template spec', () => {
	beforeEach(() => {
		cy.visit('https://example.cypress.io');
	});

	it('audits the home page', () => {
		cy.lighthouse({
			performance: 50,
			accessibility: 90,
			'best-practices': 50,
			seo: 50,
			pwa: 50,
		});
		// TODO: bring back cy.pa11y();
	});
});
