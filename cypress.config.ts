import { defineConfig } from 'cypress';
import { lighthouse, prepareAudit } from '@cypress-audit/lighthouse';
import { pa11y } from '@cypress-audit/pa11y';

export default defineConfig({
	component: {
		devServer: {
			framework: 'react',
			bundler: 'vite',
		},
	},
	e2e: {
		baseUrl: 'http://localhost:3000/',
		setupNodeEvents(on, config) {
			// implement node event listeners here
			on('before:browser:launch', (browser = {}, launchOptions) => {
				prepareAudit(launchOptions);
			});

			on('task', {
				lighthouse: lighthouse(),
				pa11y: pa11y(console.log.bind(console)),
			});
		},
		video: false,
	},
	experimentalMemoryManagement: true,
	projectId: '8ask51',
});
